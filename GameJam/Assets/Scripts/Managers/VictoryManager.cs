using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryManager : MonoBehaviour {
    public GameObject steveWin, steveLose, bellaWin, bellaLose;
    public GameObject gameManager;
    string win, lose;

    private void Awake() {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        win = gameManager.GetComponent<GameManager>().win;
        lose = gameManager.GetComponent<GameManager>().lose;
        Debug.Log("Win = " + win);
        Debug.Log("Lose = " + lose);
        switch (win) {

            case "Bella":
                steveWin.SetActive(false);
                break;
            case "Steve":
                bellaWin.SetActive(false);
                break;
        }
        switch (lose) {
            case "Bella":
                steveLose.SetActive(false);
                break;
            case "Steve":
                bellaLose.SetActive(false);
                break;
        }
    }
}
