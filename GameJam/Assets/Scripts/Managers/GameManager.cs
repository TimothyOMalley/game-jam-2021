using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(SoundManager))]
[RequireComponent(typeof(CharacterManager))]

public class GameManager : MonoBehaviour {
    private GameObject playerOne, playerTwo, playerOneHealthBar, playerTwoHealthBar, pauseIcon;
    public float playerOneHealth, playerTwoHealth;
    public Sprite arenaTex;
    public string lose, win;

    private void Awake() {
        SceneManager.sceneLoaded += SceneLoaded;
        DontDestroyOnLoad(this);
        GameObject[] managers = GameObject.FindGameObjectsWithTag("GameManager");
        if (managers.Length > 1)
            Destroy(managers[1].gameObject);
    }



    private void Update() {
        if (SceneManager.GetActiveScene().name == "Ryan") {
            //Keeps global health updated
            playerOneHealth = playerOne.GetComponent<Player>().GetHealth();
            playerTwoHealth = playerTwo.GetComponent<Player>().GetHealth();

            //Changes the size of the health bar depending on player's current/max health
            playerOneHealthBar.GetComponent<RectTransform>().localScale = new Vector3((playerOneHealth / playerOne.GetComponent<Player>().GetMaxHealth()), 1, 1);
            playerTwoHealthBar.GetComponent<RectTransform>().localScale = new Vector3((playerTwoHealth / playerTwo.GetComponent<Player>().GetMaxHealth()), 1, 1);

            if (playerOne.GetComponent<Player>().GetHealth() == 0) {
                //player 2 win
                win = playerTwo.GetComponent<Player>().character;
                lose = playerOne.GetComponent<Player>().character;
                SceneManager.LoadScene("PlayerTwoWins");
            }
            if (playerTwo.GetComponent<Player>().GetHealth() == 0) {
                //player 1 win
                win = playerOne.GetComponent<Player>().character;
                lose = playerTwo.GetComponent<Player>().character;
                SceneManager.LoadScene("PlayerOneWins");
            }
        }
    }

    private void SceneLoaded(Scene scene, LoadSceneMode mode) {
        switch (scene.name) {
            default:
                //case "Title Screen":
                //case "CharacterSelect":
                //case "Stage Select":
                //case "PlayerOneWins":
                //case "PlayerTwoWins":
                GetCursorsOrPlayers();
                break;
            case "Ryan":
                GetCursorsOrPlayers();
                SetUpBattleScene();
                break;
        }
    }

    public GameObject GetPlayer(int num) {
        if (num == 1)
            return playerOne;
        if (num == 2)
            return playerTwo;
        return null;
    }

    void GetCursorsOrPlayers() {
        //Set Scheme to CharSelect
        GetComponent<InputManager>().SetScheme(InputManager.ControlScheme.charSelect);

        playerOne = GameObject.FindGameObjectWithTag("Player One");
        playerTwo = GameObject.FindGameObjectWithTag("Player Two");
        GetComponent<InputManager>().playerOne = playerOne;
        GetComponent<InputManager>().playerTwo = playerTwo;
    }

    void SetUpBattleScene() {
        //Set Scheme to Playing
        GetComponent<InputManager>().SetScheme(InputManager.ControlScheme.playing);

        //Setup health bars
        playerOneHealthBar = GameObject.FindGameObjectWithTag("P1HealthBar");
        playerTwoHealthBar = GameObject.FindGameObjectWithTag("P2HealthBar");

        //Set player's characters
        GetComponent<CharacterManager>().SetCharacter(1);
        GetComponent<CharacterManager>().SetCharacter(2);

        //Set background
        SpriteRenderer background = GameObject.FindGameObjectWithTag("Background").GetComponent<SpriteRenderer>();
        background.sprite = arenaTex;

        //Setup pause
        pauseIcon = GameObject.FindGameObjectWithTag("Paused");
        GetComponent<InputManager>().pausedIcon = pauseIcon;
        pauseIcon.SetActive(false);

        //Setup sounds
        GetComponent<InputManager>().p1doHit = playerOne.GetComponent<Player>().doHit;
        GetComponent<InputManager>().p2doHit = playerTwo.GetComponent<Player>().doHit;
    }

    private void OnDestroy() {
        SceneManager.sceneLoaded -= SceneLoaded;
    }

}
