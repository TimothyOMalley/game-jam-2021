using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {
    AudioSource audioSource;
    [SerializeField] AudioClip titleScreen;
    [SerializeField] AudioClip fightScene;
    [SerializeField] float volume = 1f;
    bool playingArena = true;
    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
        audioSource.volume = volume;
    }

    void Update() {
        if (SceneManager.GetActiveScene().name == "Title Screen") {
            if (playingArena) {
                audioSource.clip = titleScreen;
                audioSource.Play();
                playingArena = false;
            }
        }
        if (SceneManager.GetActiveScene().name == "CharacterSelect") {

        }
        if (SceneManager.GetActiveScene().name == "Stage Select") {

        }
        if (SceneManager.GetActiveScene().name == "Ryan") {
            if (!playingArena) {
                audioSource.clip = fightScene;
                audioSource.Play();
                playingArena = true;
            }
        }
    }
}
