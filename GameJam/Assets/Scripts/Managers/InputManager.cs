using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
    public enum ControlScheme { charSelect, playing, paused }
    [SerializeField] ControlScheme currentScheme = ControlScheme.charSelect;
    public GameObject playerOne;
    public GameObject playerTwo;
    public GameObject pausedIcon;

    public AudioSource p1doHit, p2doHit;

    public bool attackingP1 = false;
    public bool attackingP2 = false;

    private void Update() {
        switch (currentScheme) {
            case ControlScheme.charSelect:
                CheckPlayerOneCharSelect();
                CheckPlayerTwoCharSelect();
                break;
            case ControlScheme.playing:
                CheckPlayerOneInput();
                CheckPlayerTwoInput();
                break;
            case ControlScheme.paused:
                CheckPlayerOnePaused();
                CheckPlayerTwoPaused();
                break;
        }
    }

    public void SetScheme(ControlScheme controlScheme) {
        currentScheme = controlScheme;
    }

    private void CheckPlayerOneCharSelect() {
        //change later to diffent movement script
        playerOne.GetComponent<CursorMovement>().UpdateInput(new Vector2(Input.GetAxis("HorizontalP1"), -Input.GetAxis("VerticalP1")));
        if (Input.GetButtonDown("A1")) {
            playerOne.GetComponent<CursorScript>().PlayerOnePressedA();
        }
        if (Input.GetButtonDown("B1")) {

        }
        if (Input.GetButtonDown("X1")) {

        }
        if (Input.GetButtonDown("Y1")) {

        }
    }

    private void CheckPlayerTwoCharSelect() {
        //Change later to differnt movement script
        playerTwo.GetComponent<CursorMovement>().UpdateInput(new Vector2(Input.GetAxis("HorizontalP2"), -Input.GetAxis("VerticalP2")));
        if (Input.GetButtonDown("A2")) {
            playerTwo.GetComponent<CursorScript>().PlayerTwoPressedA();
        }
        if (Input.GetButtonDown("B2")) {

        }
        if (Input.GetButtonDown("X2")) {

        }
        if (Input.GetButtonDown("Y2")) {

        }
    }

    private void CheckPlayerOneInput() {
        playerOne.GetComponent<CharacterMovement>().UpdateMovement(new Vector2(Input.GetAxis("HorizontalP1"), -Input.GetAxis("VerticalP1")));

        if (Input.GetButtonDown("A1")) {
            playerOne.GetComponent<Player>().APressed();
        }
        if (Input.GetButtonDown("B1")) {

        }
        if (Input.GetButtonDown("X1")) {

        }
        if (Input.GetButtonDown("Y1")) {

        }
        if (Input.GetButtonDown("P1Start")) {
            Pause();
        }
    }

    private void CheckPlayerTwoInput() {


        playerTwo.GetComponent<CharacterMovement>().UpdateMovement(new Vector2(Input.GetAxis("HorizontalP2"), -Input.GetAxis("VerticalP2")));
        if (Input.GetButtonDown("A2")) {
            playerTwo.GetComponent<Player>().APressed();
        }
        if (Input.GetButtonDown("B2")) {

        }
        if (Input.GetButtonDown("X2")) {

        }
        if (Input.GetButtonDown("Y2")) {

        }
        if (Input.GetButtonDown("P2Start")) {
            Pause();
        }
    }

    public void Pause() {
        if (currentScheme == ControlScheme.paused) {
            currentScheme = ControlScheme.playing;
            pausedIcon.SetActive(false);
        }
        else {
            currentScheme = ControlScheme.paused;
            pausedIcon.SetActive(true);
        }
    }

    public void CheckPlayerOnePaused() {
        if (Input.GetButtonDown("P1Start")) {
            Pause();
        }
    }

    public void CheckPlayerTwoPaused() {
        if (Input.GetButtonDown("P2Start")) {
            Pause();
        }
    }

    /// <summary>
    /// 0 -> ?
    /// 1 -> ?
    /// 2 -> ?
    /// 3 -> ?
    /// 4 -> ?
    /// 5 -> up pad
    /// 6 -> down pad
    /// 7 -> left pad
    /// 8 -> ?
    /// 9 -> start button
    /// 10 -> back button
    /// 11 -> left analogue press
    /// 12 -> right analogue press
    /// 13 -> left bumper
    /// 14 -> right bumper
    /// 15  -> power button
    /// 16  -> A
    /// 17 -> B
    /// 18 -> X
    /// 19 -> Y
    /// </summary>
}
