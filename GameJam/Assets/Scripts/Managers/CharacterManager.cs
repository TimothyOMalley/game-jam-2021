using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CharacterManager : MonoBehaviour {

    public bool p1_ready, p2_ready;
    public int p1_character, p2_character;

    void Update() {
        if (p1_ready == true && p2_ready == true) {
            p1_ready = false;
            p2_ready = false;
            SceneManager.LoadScene("Stage Select", LoadSceneMode.Single);

        }
    }

    public void SetCharacter(int playerNum) {
        GameObject player = GetComponent<GameManager>().GetPlayer(playerNum);
        if (playerNum == 1) {
            if (p1_character == 1)
                player.GetComponent<Player>().SetCharacter("Bella");
            if (p1_character == 2)
                player.GetComponent<Player>().SetCharacter("Steve");
        }
        if (playerNum == 2) {
            if (p2_character == 1)
                player.GetComponent<Player>().SetCharacter("Bella");
            if (p2_character == 2)
                player.GetComponent<Player>().SetCharacter("Steve");
        }
    }
}
