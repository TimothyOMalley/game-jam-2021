using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterAnimator))]
public class AnimationManager : MonoBehaviour {
    string character;
    CharacterAnimator animator;
    CharacterMovement movement;
    string lastDir = "right";
    [SerializeField] float idleDelay = 2.5f;
    float idleDelayReset;

    private void OnValidate() { idleDelayReset = idleDelay; }
    private void Start() { idleDelayReset = idleDelay; }

    public void SetLastDir(string dir) { lastDir = dir; }

    public void SetCharacter(string character) {
        this.character = character;
        animator = GetComponent<CharacterAnimator>();
        animator.SetupCharacter(character);
        movement = GetComponent<CharacterMovement>();
    }

    public void APressed() {
        if (lastDir == "right") {
            animator.AttackRight(character);
        }
        if (lastDir == "left") {
            animator.AttackLeft(character);
        }
    }

    public void RingCollision() {
        if (movement.input.x > 0) { animator.LeanRight(character); }
        if (movement.input.x < 0) { animator.LeanLeft(character); }
    }

    private void FixedUpdate() {
        if (movement.input.x > 0 && !GetComponent<CollisionManager>().onRing) {
            animator.MoveRight(character);
            lastDir = "right";
        }
        if (movement.input.x < 0 && !GetComponent<CollisionManager>().onRing) {
            animator.MoveLeft(character);
            lastDir = "left";
        }

        if (animator.CurrentAnimation(character) == character + "_Idle") {
            idleDelay -= Time.deltaTime;
            if (idleDelay <= 0)
                animator.IdleP2(character);
        }
        else {
            idleDelay = idleDelayReset;
        }

    }

}
