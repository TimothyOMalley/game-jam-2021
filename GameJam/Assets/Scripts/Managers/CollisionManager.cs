using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class CollisionManager : MonoBehaviour {

    public bool onRing = false;
    bool projectile = false;
    bool hitable = true;
    float charge = 0f;
    CharacterMovement movement;

    private void Awake() {
        movement = GetComponent<CharacterMovement>();
    }

    //when collide with other player
    private void OnCollisionEnter2D(Collision2D collision) {
        if (tag == "Player One") {
            if (collision.gameObject.tag == "Player Two" && collision.gameObject.GetComponent<CollisionManager>().projectile) {
                GetComponent<Player>().TakeDamage(20);
            }
            if (collision.collider.gameObject.tag == "HitboxP2" && hitable) {
                GetComponent<Player>().TakeDamage(10);
                hitable = false;
            }
        }
        if (tag == "Player Two") {
            if (collision.gameObject.tag == "Player One" && collision.gameObject.GetComponent<CollisionManager>().projectile) {
                GetComponent<Player>().TakeDamage(20);
            }
            if (collision.collider.gameObject.tag == "HitboxP1" && hitable) {
                GetComponent<Player>().TakeDamage(10);
                hitable = false;
            }
        }
        if (collision.gameObject.tag == "Ring_Left") {
            charge = 0;
            onRing = true;
        }
        if (collision.gameObject.tag == "Ring_Right") {
            charge = 0;
            onRing = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ring_Left") {
            GetComponent<AnimationManager>().RingCollision();
            if (charge < 500) {
                if (charge > 0) {
                    collision.collider.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/rings/ring2");
                }
                if (charge > 100) {
                    collision.collider.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/rings/ring3");
                }
                charge += 10;
            }
        }
        if (collision.gameObject.tag == "Ring_Right") {
            GetComponent<AnimationManager>().RingCollision();
            if (charge > -500) {
                if (charge < 0) {
                    collision.collider.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/rings/ring5");
                }
                if (charge < -100) {
                    collision.collider.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/rings/ring6");
                }
                charge -= 10;
            }
        }
        if (onRing && movement.input.x == 0) {
            projectile = true;
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(charge, 0));
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.tag == "Ring_Left") {
            onRing = false;
            collision.collider.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/rings/ring1");
            if (projectile)
                GetComponent<AnimationManager>().SetLastDir("right");
        }
        if (collision.gameObject.tag == "Ring_Right") {
            onRing = false;
            collision.collider.GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/rings/ring4");
            if (projectile)
                GetComponent<AnimationManager>().SetLastDir("left");
        }
        if (tag == "Player One") {
            if (collision.collider.gameObject.tag == "HitboxP2") {
                hitable = true;
            }
        }
        if (tag == "Player Two") {
            if (collision.collider.gameObject.tag == "HitboxP1") {
                hitable = true;
            }
        }
    }

    private void FixedUpdate() {
        if (projectile && charge > 0) { charge -= 10; }
        if (projectile && charge < 0) { charge += 10; }
        if (projectile && charge == 0) { projectile = false; }
    }
}
