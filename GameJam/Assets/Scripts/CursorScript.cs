using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CursorMovement))]
[RequireComponent(typeof(BoxCollider2D))]
public class CursorScript : MonoBehaviour {
    bool hover = false;
    int character_index, arena_index, menu_item;
    bool main_menu;

    Sprite selected;

    public GameObject gameManager;

    // Start is called before the first frame update
    void Start() {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }

    public void PlayerOnePressedA() {
        if (hover && gameObject.tag == "Player One") {
            if (main_menu) {
                SceneManager.LoadScene("Title Screen");
            }
            if (SceneManager.GetActiveScene().name == "CharacterSelect") {
                gameManager.GetComponent<CharacterManager>().p1_character = character_index;
                gameManager.GetComponent<CharacterManager>().p1_ready = true;
            }
            if (SceneManager.GetActiveScene().name == "Stage Select") {
                switch (arena_index) {
                    case 1:
                        selected = Resources.Load<Sprite>("Sprites/Background/Night");
                        break;
                    case 2:
                        selected = Resources.Load<Sprite>("Sprites/Background/Sunset");
                        break;
                    case 3:
                        selected = Resources.Load<Sprite>("Sprites/Background/Regal");
                        break;
                    case 4:
                        selected = Resources.Load<Sprite>("Sprites/Background/Pastel");
                        break;
                }
                gameManager.GetComponent<GameManager>().arenaTex = selected;
                SceneManager.LoadScene("Ryan", LoadSceneMode.Single);
            }
            if (SceneManager.GetActiveScene().name == "Title Screen") {
                switch (menu_item) {
                    case 1:
                        SceneManager.LoadScene("CharacterSelect", LoadSceneMode.Single);
                        break;
                    case 2:
                        break;
                    case 3:
                        Application.Quit();
                        break;
                }
            }
            gameObject.SetActive(false);
        }
    }

    public void PlayerTwoPressedA() {
        if (hover && gameObject.tag == "Player Two") {
            if (main_menu) {
                SceneManager.LoadScene("Title Screen");
            }
            if (SceneManager.GetActiveScene().name == "CharacterSelect") {
                gameManager.GetComponent<CharacterManager>().p2_character = character_index;
                gameManager.GetComponent<CharacterManager>().p2_ready = true;
                gameObject.SetActive(false);
            }
        }
    }


    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.gameObject.name == "Bella_Square") {
            hover = true;
            character_index = 1;
            //Debug.Log("Collide Bella!");
        }
        if (collision.gameObject.name == "Steve_Square") {
            hover = true;
            character_index = 2;
            //Debug.Log("Collide Steve!");
        }

        if (collision.gameObject.name == "Night Arena") {
            hover = true;
            arena_index = 1;
        }
        if (collision.gameObject.name == "Sunset Arena") {
            hover = true;
            arena_index = 2;
        }
        if (collision.gameObject.name == "Regal Arena") {
            hover = true;
            arena_index = 3;
        }
        if (collision.gameObject.name == "Pastel Arena") {
            hover = true;
            arena_index = 4;
        }


        if (collision.gameObject.name == "Main Menu") {
            main_menu = true;
            hover = true;
        }


        if (collision.gameObject.name == "Play Button") {
            hover = true;
            menu_item = 1;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Title Screen/play_alt");
            ;
        }
        if (collision.gameObject.name == "Controls") {
            hover = true;
            menu_item = 2;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Title Screen/controls_alt");
            ;
        }
        if (collision.gameObject.name == "Quit") {
            hover = true;
            menu_item = 3;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Title Screen/quit_alt");
            ;
        }

    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.name == "Bella_Square" || collision.gameObject.name == "Steve_Square") {
            character_index = 0;
            hover = false;
        }
        if (collision.gameObject.name == "Night Arena" || collision.gameObject.name == "Sunset Arena" || collision.gameObject.name == "Regal Arena" || collision.gameObject.name == "Pastel Arena") {
            arena_index = 0;
            hover = false;
        }
        if (collision.gameObject.name == "Play Button") {
            menu_item = 0;
            hover = false;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Title Screen/Play Button");
        }
        if (collision.gameObject.name == "Controls") {
            menu_item = 0;
            hover = false;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Title Screen/Controls Button");
        }
        if (collision.gameObject.name == "Quit") {
            menu_item = 0;
            hover = false;
            collision.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Title Screen/Quit Button");
            ;
        }
        if (collision.gameObject.name == "Main Menu") {
            main_menu = false;
            hover = false;
        }
    }
}
