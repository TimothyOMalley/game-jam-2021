using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour {
    public GameObject GameManager;
    public Animator animator;
    string[] clipNames = new string[] { "_Walk_Right", "_Walk_Left", "_Attack_Left", "_Attack_Right", "_Lean_Left", "_Lean_Right", "_Idle", "_IdleP2" };

    public void SetupCharacter(string character) {
        animator = GetComponent<Animator>();
        animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animation/" + character + "_Animator");
    }

    public void MoveRight(string character) {
        // transform.localScale = new Vector2(-1, 0);
        animator.Play(character + "_Walk_Right");
    }
    public void MoveLeft(string character) {
        //transform.localScale = new Vector2(1, 0);
        animator.Play(character + "_Walk_Left");
    }
    public void AttackLeft(string character) {
        animator.Play(character + "_Attack_Left");
        Invoke("StopAnimation", 0.4f);
    }
    public void AttackRight(string character) {
        animator.Play(character + "_Attack_Right");
        Invoke("StopAnimation", 0.4f);
    }
    public void LeanLeft(string character) {
        animator.Play(character + "_Lean_Left");
    }
    public void LeanRight(string character) {
        animator.Play(character + "_Lean_Right");
    }
    public void StopAnimation() {
        animator.StopPlayback();
    }
    public void IdleP2(string character) {
        animator.Play(character + "_IdleP2");
    }

    public void FinishAttack() {

    }

    public string CurrentAnimation(string character) {
        for (int i = 0; i < clipNames.Length; i++) {
            if (animator.GetCurrentAnimatorStateInfo(0).IsName(character + clipNames[i])) {
                return character + clipNames[i];
            }
        }
        return "";
    }
}
