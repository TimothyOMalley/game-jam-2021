using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CursorMovement : MonoBehaviour {
    Vector2 input;
    Rigidbody2D rigidBody;
    [SerializeField] float speed = 50f;

    public void UpdateInput(Vector2 input) { this.input = input * speed; }

    private void Awake() {
        input = Vector2.zero;
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.gravityScale = 0f;
        rigidBody.drag = 10f;
        rigidBody.freezeRotation = true;
    }

    private void FixedUpdate() { rigidBody.AddForce(input); }
}
