using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterMovement : MonoBehaviour {

    Rigidbody2D rigidBody;
    public Vector2 input = Vector2.zero;
    [SerializeField] float speed = 20f;

    private void Start() { //setup stuff
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.gravityScale = 0;
        rigidBody.drag = 0.5f;
        rigidBody.freezeRotation = true;
    }

    public void UpdateMovement(Vector2 input) { //get player input
        this.input = input * speed;
    }

    private void FixedUpdate() {
        rigidBody.AddForce(input); //add force on fixed
    }
}
