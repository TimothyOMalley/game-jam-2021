using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterMovement))]
[RequireComponent(typeof(AnimationManager))]
[RequireComponent(typeof(CollisionManager))]
public class Player : MonoBehaviour {
    [SerializeField] float maxHealth, currentHealth;
    [SerializeField] AudioSource goHit;
    [SerializeField] AudioClip gotHit, steveHit, bellaHit;
    [SerializeField] AnimationManager animator;
    public AudioSource doHit;
    [SerializeField] bool projectile;

    public string animationClass;
    public string character;

    public void APressed() {
        doHit.Play();
        animator.APressed();
    }

    public void SetCharacter(string charName) {
        if (charName == "Bella") {
            character = "Bella";
            doHit.clip = bellaHit;
        }
        if (charName == "Steve") {
            character = "Steve";
            doHit.clip = steveHit;
        }
        goHit.clip = gotHit;
        animator.SetCharacter(character);
    }

    private void Awake() { //sets up initially 
        animator = GetComponent<AnimationManager>();
        doHit.loop = false;
        goHit.loop = false;
    }

    public void TakeDamage(float damage) {
        currentHealth -= damage;
        if (currentHealth <= 0) {
            currentHealth = 0;
        }
        goHit.Play();
    }

    public float GetHealth() { return currentHealth; }

    public float GetMaxHealth() { return maxHealth; }
}
