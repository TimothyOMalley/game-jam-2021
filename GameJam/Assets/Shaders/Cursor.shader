Shader "Unlit/Cursor"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "black" {}
        _ColorThreshhold ("Color Threshhold",Color) = (0.5, 0.5, 0.5, 1.0)
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" }
        //LOD 100

        //Grab screen behind the object
        GrabPass
        {
            "_BackgroundText"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f{
                half4 grabPos : TEXCOORD0;
                half4 pos : SV_POSITION;
            };

            sampler2D _BackgroundText;
            half4 _ColorThreshhold;

            v2f vert (appdata_base v){
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.grabPos = ComputeGrabScreenPos(o.pos);
                return o;
            }

            half4 frag (v2f i) : SV_Target{
                half4 bgcolor = tex2Dproj(_BackgroundText, i.grabPos);
                return 1-bgcolor;
            }
            ENDCG
        }
    }
}
